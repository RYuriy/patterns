let Category = [
    {id: 1, name: "Questions"},
    {id: 2, name: "Common phrases"},
    {id: 3, name: "Irregular verbs"},
    {id: 4, name: "Verbs"},
];
let Data = [
    {category: 1, ru: "Как Вас зовут?", en: "What is your name?"},
    {category: 1, ru: "Как я могу к Вам обращаться?", en: "How can I address you?"},
    {category: 1, ru: "Могу я Вам помочь?", en: "Can I help you?"},
    {category: 1, ru: "Как я могу Вам помочь?", en: "How can I help you?"},
    {category: 1, ru: "Что это?", en: "What is it?"},
    {category: 1, ru: "Что такое \"jogging\"?", en: "What is \"jogging\"?"},
    {category: 1, ru: "Как \"привет\" будет на английском?", en: "What is the English for \"привет\"?"},
    {category: 1, ru: "Как \"bye\" будет на русском?", en: "How do you say \"bye\"  in Russian?"},
    {category: 1, ru: "Как правильно сказать \"помогите\" на английском?", en: "How do you say \"помогите\" in English?"},
    {category: 1, ru: "Как правильно произносится на английском \"jogging\"?", en: "How is \"jogging\"correctly pronounced in English?"},
    {category: 1, ru: "Как это пишится?", en: "How do you write it?"},
    {category: 1, ru: "Как пишится слово \"language\"?", en: "How do you write the word \"language\"?"},
    {category: 1, ru: "Как это слово произносится по буквам?", en: "How do you spell this word?"},
    {category: 1, ru: "Что случилось?", en: "What's  happened? What's up?"},
    {category: 1, ru: "Что происходит?", en: "What's goining on?"},
    {category: 1, ru: "Что Вы хотите?", en: "What do you want?"},
    {category: 1, ru: "Что бы Вы хотели?", en: "What would you like?"},
    {category: 1, ru: "Что бы Вы хотели поесть?", en: "What would you like to eat?"},
    {category: 1, ru: "Что бы Вы хотели выпить?", en: "What would you like to drink?"},
    {category: 1, ru: "Что бы Вы хотели увидеть?", en: "What would you like to see?"},
    {category: 1, ru: "Что бы Вы хотели показать мне/нам?", en: "What would you like to show me / us?"},
    {category: 1, ru: "Куда бы Вы хотели пойти?", en: "Where would you like to go?"},
    {category: 1, ru: "Куда Вы идете?", en: "Where are you going?"},
    {category: 1, ru: "Что вы делаете?", en: "What are you doing?"},
    {category: 1, ru: "Как ты это сделал?", en: "How did you do this?"},
    {category: 1, ru: "Зачем вы это делаете?", en: "Why are you doing it?"},
    {category: 1, ru: "Как это работает?", en: "How does it work?"},
    {category: 1, ru: "Как это включить?", en: "How can I turn it on?"},
    {category: 1, ru: "Как это выключить?", en: "How can I turn it off?"},
    {category: 1, ru: "Как это переключить?", en: "How can I switch it?"},
    {category: 1, ru: "Как это сделать?", en: "How to do it?"},
    {category: 1, ru: "Как это лучше сделать?", en: "What is the best way to do it?"},
    {category: 1, ru: "Как правильно это сделать?", en: "How can you do it right?"},
    {category: 1, ru: "Что я должен сделать?", en: "What I should do?"},
    {category: 1, ru: "Я должен позвонить ему?", en: "Should I call him?"},
    {category: 1, ru: "Я должен сказать ей?", en: "Should I tell her?"},
    {category: 1, ru: "Что я должен сказать им?", en: "What should I tell them?"},
    {category: 1, ru: "Кто это?", en: "Who is it?"},
    {category: 1, ru: "Как его зовут?", en: "What's his name?"},
    {category: 1, ru: "Как имя того парня?", en: "What's the name of that guy?"},
    {category: 1, ru: "Кто пришел?", en: "Who's come?"},
    {category: 1, ru: "Кто ушел?", en: "Who left?"},
    {category: 1, ru: "Где вход в здание?", en: "Where is the entrance to the building?"},
    {category: 1, ru: "Где выход ?", en: "Where is the exit ?"},
    {category: 1, ru: "Где это находится?", en: "Where is it?"},
    {category: 1, ru: "Как туда пройти/проехать?", en: "How to go there?"},
    {category: 1, ru: "На чем можно туда добраться?", en: "How can I get there?"},
    {category: 1, ru: "Это далеко?", en: "It is far?"},
    {category: 1, ru: "Когда улетает самолет?", en: "When does an airplane depart?"},
    {category: 1, ru: "Когда отправляется поезд?", en: "When does the train leave?"},
    {category: 1, ru: "Когда прибывает поезд?", en: "When does the train arrive?"},
    {category: 1, ru: "Когда открывается этот магазин?", en: "When does this store open?"},
    {category: 1, ru: "Когда открываются музеи?", en: "When do museums open?"},
    {category: 1, ru: "Когда закрывается этот банк?", en: "When does this bank close?"},
    {category: 1, ru: "Когда закрываются рестораны?", en: "When do restaurants close?"},
    {category: 1, ru: "Куда едет этот автобус?", en: "Where does this bus go?"},
    {category: 1, ru: "Где я могу найти мэнеджера?", en: "Where can I find a manager?"},
    {category: 1, ru: "Где ваш начальник?", en: "Where is your boss?"},
    {category: 1, ru: "Где отдел разработки?", en: "Where is the development department?"},
    {category: 1, ru: "Кто-то видел мою чашку?", en: "Has anybody seen my cup?"},
    {category: 1, ru: "Ты уже видел мою новую машину?", en: "Have you seen my new car?"},
    {category: 1, ru: "Кто-нибудь звонил мне?", en: "Has anyone called me?"},
    {category: 1, ru: "Мне не оставляли сообщения?", en: "Has anyone left messages for me?"},
    {category: 1, ru: "Ты получал мое сообщение?", en: "Have you received my message?"},
    {category: 1, ru: "Ты уже получил мое сообщение?", en: "Have you already received my message?"},
    {category: 1, ru: "Ты уже сделал эту задачу?", en: "Have you already done this task?"},
    {category: 1, ru: "Ты уже начал делать эту задачу?", en: "Have you already begun to do this task?"},
    {category: 1, ru: "Чья очередь говорить?", en: "Whose turn is it to talk?"},
    {category: 1, ru: "Чья очередь читать?", en: "Whose turn is it to read?"},
    {category: 1, ru: "Ты когда-нибудь был в Аргентине?", en: "Have you ever been to Argentina?"},
    {category: 1, ru: "Ты когда-нибудь летал на самолете?", en: "Have you ever flown on an airplane?"},
    {category: 1, ru: "Ты когда-нибудь работал в этой фирме?", en: "Have you ever worked in this company?"},
    {category: 1, ru: "Сколько лет ты живешь в Одессе?", en: "How many years have you lived in Odessa?"},
    {category: 1, ru: "Как долго ты здесь работаешь?", en: "How long have you been working here?"},
    {category: 1, ru: "Сколько лет ты здесь работаешь?", en: "How many years have you been working here?"},
    {category: 1, ru: "Что ты будешь делать вечером?", en: "What are you going to do this evening?"},
    {category: 1, ru: "Что ты делаешь завтра?", en: "What are you doing tomorrow?"},
    {category: 1, ru: "Что ты деллаешь в эти выходные?", en: "What are you doing this weekend?"},
    {category: 1, ru: "Что ты делаешь по выходным", en: "What do you do on weekends?"},
    {category: 1, ru: "Что вы предпочитаете?", en: "What do you prefer?"},
    {category: 1, ru: "Что вы порекомендуете?", en: "What do you recommend?"},
    {category: 1, ru: "Что вы об этом думаете?", en: "What do you think about it?"},
    {category: 1, ru: "Можно вас спросить?", en: "May I ask you?"},
    {category: 1, ru: "Когда ты собираешься в отпуск?", en: "When are you going on vacation?"},

    {category: 2, ru: "Как это сказать…", en: "How to say…"},
    {category: 2, ru: "Скажите мне пожалуйста", en: "Tell me please…"},
    {category: 2, ru: "Не могли бы вы повторить это?", en: "Could you repeat that please?"},
    {category: 2, ru: "Я вас не понял", en: "I did not understand you"},
    {category: 2, ru: "Я не совсем понял вас", en: "I have not quite understood you"},
    {category: 2, ru: "Простите, я не понимаю", en: "Sorry, I do not understand"},
    {category: 2, ru: "Простите, не могли бы вы говорить медленнее", en: "Sorry, could you speak more slowly?"},
    {category: 2, ru: "Простите, не могли бы вы говорить не так быстро", en: "Sorry, could you speak not so quickly"},
    {category: 2, ru: "Я не знаю как это сказать на английском", en: "I do not know how to say it in English"},
    {category: 2, ru: "Я не знаю как это пишится", en: "I do not know how it's written"},
    {category: 2, ru: "Простите, Мне нужна помощь", en: "Excuse me, I need help."},
    {category: 2, ru: "Помогите мне пожалуйста", en: "help me please"},
    {category: 2, ru: "Простите, не моглибы вы мне помочь, пожалуйста?", en: "Excuse me, could you help me, please?"},
    {category: 2, ru: "Собственно говоря…", en: "Actually"},
    {category: 2, ru: "На самом деле…", en: "As a matter of fact…"},
    {category: 2, ru: "В основном…", en: "Basically… / Generally…"},
    {category: 2, ru: "Я думаю это надо сделать так", en: "I think it should be done this way"},
    {category: 2, ru: "По моему мнению, это неправильно", en: "In my opinion, this is wrong"},
    {category: 2, ru: "Это правильно", en: "It is right"},
    {category: 2, ru: "Это ошибка", en: "It is a mistake"},
    {category: 2, ru: "Это не правильно", en: "It is not right"},
    {category: 2, ru: "Я согласен с вами", en: "I agree with you"},
    {category: 2, ru: "Я не согласен с вами", en: "I disagree with you"},
    {category: 2, ru: "Это не имеет значения", en: "It does not matter"},
    {category: 2, ru: "Это зависит от обстоятельств", en: "It depends on the circumstances / It depends"},

    {category: 3, ru: "быть", en: "be, was/were, been"},
    {category: 3, ru: "иметь", en: "have, had, had"},
    {category: 3, ru: "делать", en: "do, did, done"},
    {category: 3, ru: "говорить", en: "say, said, said"},
    {category: 3, ru: "идти", en: "go, went, gone"},
    {category: 3, ru: "получать", en: "get, got, got/gotten"},
    {category: 3, ru: "делать/изготавливать", en: "make, made, made"},
    {category: 3, ru: "знать", en: "know, knew, known"},
    {category: 3, ru: "думать", en: "think, thought, thought"},
    {category: 3, ru: "брать", en: "take, took, taken"},
    {category: 3, ru: "видеть", en: "see, saw, seen"},
    {category: 3, ru: "приходить", en: "come, came, come"},
    {category: 3, ru: "хотеть", en: "want, wanted, wanted"},
    {category: 3, ru: "использовать", en: "use, used, used"},
    {category: 3, ru: "находить", en: "find, found, found"},
    {category: 3, ru: "давать", en: "give, gave, given"},
    {category: 3, ru: "рассказывать", en: "tell, told, told"},
    {category: 3, ru: "работать", en: "work, worked, worked"},
    {category: 3, ru: "звать; звонить", en: "call, called, called"},
    {category: 3, ru: "пытаться", en: "try, tried, tried"},
    {category: 3, ru: "просить; спрашивать", en: "ask, asked, asked"},
    {category: 3, ru: "нуждаться", en: "need, needed, needed"},
    {category: 3, ru: "чувствовать", en: "feel, felt, felt"},
    {category: 3, ru: "становиться", en: "become, became, become"},
    {category: 3, ru: "оставлять", en: "leave, left, left"},
    {category: 3, ru: "класть; ставить", en: "put, put, put"},
    {category: 3, ru: "значить", en: "mean, meant, meant"},
    {category: 3, ru: "хранить", en: "keep, kept, kept"},
    {category: 3, ru: "позволять", en: "let, let, let"},
    {category: 3, ru: "начинать", en: "begin, began, begun"},
    {category: 3, ru: "казаться", en: "seem, seemed, seemed"},
    {category: 3, ru: "помогать", en: "help, helped, helped"},
    {category: 3, ru: "показывать", en: "show, showed, shown"},
    {category: 3, ru: "слышать", en: "hear, heard, heard"},
    {category: 3, ru: "играть", en: "play, played, played"},
    {category: 3, ru: "бежать", en: "run, run, run"},
    {category: 3, ru: "двигаться", en: "move, moved, moved"},
    {category: 3, ru: "жить", en: "live, lived, lived"},
    {category: 3, ru: "верить", en: "believe, believed, believed"},
    {category: 3, ru: "приносить", en: "bring, brought, brought"},
    {category: 3, ru: "случаться", en: "happen, happened, happened"},
    {category: 3, ru: "писать", en: "write, wrote, written"},
    {category: 3, ru: "сидеть", en: "sit, sat, sat"},
    {category: 3, ru: "стоять", en: "stand, stood, stood"},
    {category: 3, ru: "терять", en: "lose, lost, lost"},
    {category: 3, ru: "платить", en: "pay, paid, paid"},
    {category: 3, ru: "встречать", en: "meet, met, met"},
    {category: 3, ru: "включать(в себя)", en: "include, included, included"},
    {category: 3, ru: "продолжать", en: "continue, continued, continued"},
    {category: 3, ru: "устанавливать", en: "set, set, set"},
    {category: 3, ru: "учить", en: "learn, learnt/learned, learnt/learned"},
    {category: 3, ru: "менять", en: "change, changed, changed"},
    {category: 3, ru: "вести", en: "lead, led, led"},
    {category: 3, ru: "понимать", en: "understand, understood, understood"},
    {category: 3, ru: "смотреть", en: "watch, watched, watched"},
    {category: 3, ru: "следовать", en: "follow, followed, followed"},
    {category: 3, ru: "останавливать", en: "stop, stopped, stopped"},
    {category: 3, ru: "создавать", en: "create, created, created"},
    {category: 3, ru: "говорить", en: "speak, spoke, spoken"},
    {category: 3, ru: "читать", en: "read, read, read"},
    {category: 3, ru: "тратить", en: "spend, spent, spent"},
    {category: 3, ru: "расти", en: "grow, grew, grown"},
    {category: 3, ru: "открывать", en: "open, opened, opened"},
    {category: 3, ru: "идти", en: "walk, walked, walked"},
    {category: 3, ru: "побеждать", en: "win, won, won"},
    {category: 3, ru: "учить", en: "teach, taught, taught"},
    {category: 3, ru: "предлагать", en: "offer, offered, offered"},
    {category: 3, ru: "помнить", en: "remember, remembered, remembered"},
    {category: 3, ru: "считать/рассматривать", en: "consider, considered, considered"},
    {category: 3, ru: "появляться", en: "appear, appeared, appeared"},
    {category: 3, ru: "покупать", en: "buy, bought, bought"},
    {category: 3, ru: "служить", en: "serve, served, served"},
    {category: 3, ru: "умирать", en: "die, died, died"},
    {category: 3, ru: "посылать", en: "send, sent, sent"},
    {category: 3, ru: "строить", en: "build, built, built"},
    {category: 3, ru: "оставаться", en: "stay, stayed, stayed"},
    {category: 3, ru: "падать", en: "fall, fell, fallen"},
    {category: 3, ru: "резать", en: "cut, cut, cut"},
    {category: 3, ru: "достигать", en: "reach, reached, reached"},
    {category: 3, ru: "убивать", en: "kill, killed, killed"},
    {category: 3, ru: "поднимать", en: "raise, raised, raised"},
    {category: 3, ru: "миновать/передавать", en: "pass, passed, passed"},
    {category: 3, ru: "продавать", en: "sell, sold, sold"},
    {category: 3, ru: "решать", en: "decide, decided, decided"},
    {category: 3, ru: "возвращаться", en: "return, returned, returned"},
    {category: 3, ru: "объяснять", en: "explain, explained, explained"},
    {category: 3, ru: "надеяться", en: "hope, hoped, hoped"},
    {category: 3, ru: "развивать/разрабатывать", en: "develop, hoped, hoped"},
    {category: 3, ru: "развивать/разрабатывать", en: "develop, developed, developed"},
    {category: 3, ru: "везти; нести", en: "carry, carried, carried"},
    {category: 3, ru: "ломать", en: "break, broke, broken"},
    {category: 3, ru: "получать", en: "receive, received, received"},
    {category: 3, ru: "соглашаться", en: "agree, agreed, agreed"},
    {category: 3, ru: "поддерживать", en: "support, supported, supported"},
    {category: 3, ru: "ударять", en: "hit, hit, hit"},
    {category: 3, ru: "производить", en: "produce, produced, produced"},
    {category: 3, ru: "есть", en: "eat, ate, eaten"},
    {category: 3, ru: "покрывать", en: "cover, covered, covered"},
    {category: 3, ru: "ловить", en: "catch, caught, caught"},
    {category: 3, ru: "рисовать", en: "draw, drew, drawn"},
    {category: 3, ru: "выбирать", en: "choose, chose, chosen"},

    {category: 4, ru: "выполнять/завершать", en: "accomplish"},
    {category: 4, ru: "действовать/влиять", en: "act"},
    {category: 4, ru: "применять/адаптировать", en: "adapt"},
    {category: 4, ru: "вести дела/обеспечивать", en: "administer"},
    {category: 4, ru: "продвигаться вперед/развиваться", en: "advance"},
    {category: 4, ru: "консультировать/советовать", en: "advise"},
    {category: 4, ru: "выделять/распределять/назначать", en: "allocate"},
    {category: 4, ru: "анализировать", en: "analyze"},
    {category: 4, ru: "применять/обращаться", en: "apply"},
    {category: 4, ru: "одобрить/санкционировать", en: "approve"},
    {category: 4, ru: "выносить решение/разбираться", en: "arbitrate"},
    {category: 4, ru: "организовывать/готовить", en: "arrange"},
    {category: 4, ru: "помогать/содействовать", en: "assist"},
    {category: 4, ru: "выполнять/доводить до конца", en: "carry out"},
    {category: 4, ru: "систематизировать", en: "catalogue"},
    {category: 4, ru: "классифицировать", en: "classify"},
    {category: 4, ru: "сравнивать", en: "compare"},
    {category: 4, ru: "заканчивать/завершать", en: "complete"},
    {category: 4, ru: "вычислять/делать выкладки", en: "compute"},

];

